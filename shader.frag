/* shader.frag
 * 
 * Real-Time Rendering & 3D Games Programming
 * Semester 2 2015 Assignment #2
 * Full Name        : Alexander Perrin
 * Student Number   : s3333342
 * Program Code     : BP214
 */

#extension GL_EXT_gpu_shader4 : require

// Uniforms
uniform vec4 _Diffuse; // Diffuse color
uniform vec4 _Spec; // Specular color
uniform vec4 _Ambient; // Ambient color
uniform bool _BlinnPhong; // Blinn-Phong / Phong lighting toggle
uniform bool _PointLighting; // Point lighting toggle
uniform bool _VertexLight; // Vertex lighting toggle
uniform bool _Lighting; // Lighting toggle
uniform bool _Normals; // Normal visualisation toggle
uniform bool _FlatShade; // Flad shading toggle
uniform float _Gloss; // Glossiness

flat varying vec3 fNormal; // Flat in normal
varying vec3 normal; // Smooth in normal
varying vec4 lightDir; // In directional light dir
varying vec4 ePos; // In eye position
varying vec4 vColor;

vec4 blinnPhongSpec(vec3 n, vec3 l, vec3 e)
{
  vec3 h = normalize(l + e);
  float intSpec = max(dot(h,n), 0.0);  
  return _Spec * pow(intSpec, _Gloss);
}

vec4 phongSpec(vec3 n, vec3 l, vec3 e)
{
  vec3 r = 2.0 * dot(l, n) * n - l;
  float intSpec = max(dot(r,e), 0.0);  
  return _Spec * pow(intSpec, _Gloss);
}

void main (void)
{
  // Just return vertex lighting color output
  if (_VertexLight)
  {
    gl_FragColor = vColor;
    return;
  }
  
  // Do some pixel lighting
  
  vec3 n = vec3(0);
  if (_FlatShade)
    n = normalize(normal);
  else
    n = normalize(fNormal);
  vec3 l = vec3(lightDir);
  vec3 e = vec3(0.0, 0.0, 1.0);
  
  // Return then normals calculation if normal vis enabled
  if (_Normals)
  {
    gl_FragColor = vec4(n, 1);
    return;
  }
  
  // Return diffuse if lighting not enabled
  if (!_Lighting)
  {
    gl_FragColor = _Diffuse;
    return;
  }
  
  float intensity = max(dot(n,l), 0.0);
  vec4 spec = vec4(0);
  
  if (intensity > 0.0)
  {
    if (_BlinnPhong)
    {
      spec = blinnPhongSpec(n, l, e);
    }
    else
    {
      spec = phongSpec(n, l, e);
    }
  }
  gl_FragColor = _Diffuse * intensity + spec + _Ambient * _Ambient;
}
