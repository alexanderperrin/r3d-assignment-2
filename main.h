/* main.h
 * 
 * Real-Time Rendering & 3D Games Programming
 * Semester 2 2015 Assignment #2
 * Full Name        : Alexander Perrin
 * Student Number   : s3333342
 * Program Code     : BP214
 */

#ifndef MAIN_H
#define MAIN_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#if _WIN32
#   include <Windows.h>
#endif
#if __APPLE__
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
#   include <GLUT/glut.h>
#else
#   include <GL/gl.h>
#   include <GL/glu.h>
#   include <GL/glut.h>
#endif

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include "shaders.h"

#define BUFFER_OFFSET(i) ((void*)(i))
#define VERTEX_BUF 0
#define INDEX_BUF 1
#define NUM_BUFFERS 2

typedef struct vertex {
	glm::vec3 pos;
	glm::vec3 normal;
} Vertex;

typedef struct {
  int vCount;
  int iCount;
  Vertex* verts;
  int* indices;
  float animTime;
} GridData;

typedef struct {
  int waveDim;
  bool animate;
  float t, lastT, realT;
  bool wireframe;
  bool lighting;
  bool fixed;
  bool drawNormals;
  int width, height;
  int tess;
  int frameCount;
  float frameRate;
  float displayStatsInterval;
  int lastStatsDisplayT;
  int osdType;
  bool consolePM;
  int shaderProgram;
  bool pixelLighting;
  bool smoothShade;
  bool pointLight;
  bool blinn;
} Global;

typedef struct material {
  glm::vec4 diffuse;
  glm::vec4 spec;
  glm::vec4 amb;
  float gloss;
} Material;

typedef enum {
  d_drawSineWave,
  d_mouse,
  d_key,
  d_animation,
  d_lighting,
  d_OSD,
  d_computeLighting,
  d_nflags
} DebugFlags;

typedef enum {
	inactive,
	rotate,
	pan,
	zoom
} CameraControl;

void drawAxes(float length);
void drawSineWave(int tess);
void drawVector(glm::vec3 r, glm::vec3 v);

#endif
