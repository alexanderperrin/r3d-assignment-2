########################################################################
# Real-Time Rendering & 3D Games Programming
# Semester 2 2015 Assignment #2
# Full Name        : Alexander Perrin
# Student Number   : s3333342
# Program Code     : BP214
########################################################################

CC = gcc
OSTYPE = darwin
OBJS = main.o shaders.o
USER = s3333342
ODIR = obj

# Linux (default)
TARGET = sinewave3D
LDFLAGS = -lGL -lGLU -lm -lglut -lGLEW -lstdc++
CFLAGS = -Wall

# Windows (cygwin)
ifeq "$(OS)" "Windows_NT"
TARGET = sinewave3D.exez
CFLAGS += -D_WIN32
endif

# OS X
ifeq "$(OSTYPE)" "darwin"
LDFLAGS = -framework Carbon -framework OpenGL -framework GLUT -lc++
CFLAGS += -D__APPLE__ -Wno-deprecated-declarations
endif

# Target compilation
sinewave: $(OBJS)
	$(CC) $(LDFLAGS) -o $(TARGET) $(OBJS) $(CFLAGS)
	make clean

# Utilities
run:
	./sinewave3D
clean:
	rm -f *.o $(TARGETS) ~*
valgrind:
	valgrind --leak-check=full --show-reachable=yes --track-origins=yes \
	./$(TARGET)
gdb:
	gdb ./$(TARGET)
