* Real-Time Rendering & 3D Games Programming
* Semester 2 2015 Assignment #2
* Full Name        : Alexander Perrin
* Student Number   : s3333342
* Program Code     : BP214

Features:
- Per pixel and per vertex lighting
- Blinn-Phong and Phong reflectance models
- Directional and point lighting
- Wireframe and solid shading
- Flat and smooth shading (per pixel only)
- Animation
- Glossiness slider
- Normal rendering

Controls:
- All as per stated in assignment specification.

Questions:
1. There is no diffrence between fixed pipeline and programmable pipeline rendering.
There should not be any notable difference between the two as they use the very same mathematical calulations.
It is likely that the fixed pipeline is using shaders under the hood, so even at hardware they are very similar.
2. When rendering at a tesselation of 2048 without animation, there is a performance increase of approximately 100% when moving from fixed to shader pipeline.
When animation is enabled, there is a performance increase of approximately 1000%.
3. There is virtually no performance difference between animated and static geometry when rendering using a vertex shader.
This is because the vertex shaders are recalculating the sine wave values every frame. The trouble required for precomputing the values for buffer storage does not outweigh the minimal performance gain.
4. There is virtually no difference in performance when rendering per pixel and per vertex. This is likely because the graphics cards running on the Sutherland machines are architectured for the more modern pixel lighting approach.
5. The main visual difference between Phong and Blinn-Phong lighting is the size of the specular highlight.
The Phong reflectance model is a far more accurate representation of specular reflection. The greater size of the Blinn-Phong highlight is due to innacuracies in the reflection calculation.
