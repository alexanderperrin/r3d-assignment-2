/* main.cpp
 *
 * Real-Time Rendering & 3D Games Programming
 * Semester 2 2015 Assignment #2
 * Full Name        : Alexander Perrin
 * Student Number   : s3333342
 * Program Code     : BP214
 */

#define GL_GLEXT_PROTOTYPES

#include "main.h"

bool debug[d_nflags] = { false, false, false, false, false, false, false };

Global g = {
  1, // wave dimensions
  false, // animate
  0.0, // t
  0.0, // lastT
  0.0, // realT
  false, // wireframe
  true, // lighting
  true, // fixed pipeline
  false, // draw normals
  0, // s width
  0, // s height
  8, // tess
  0, // frame count
  0.0, // frame rate
  1.0, // display stats interval
  0, // lastStatsDisplayT
  0, // OSD
  false, // CPM
  0, // shader program
  false, // pixel lighting
  true, // smoothShade
  false, // point lighting
  true // blinn lighting model
};

struct camera_t {
  int lastX, lastY;
  float rotateX, rotateY;
  float scale;
  CameraControl control;
} camera = { 0, 0, 0.0, 0.0, 1.0, inactive };

const float milli = 1000.0;

static Material sineWaveMat = {
  glm::vec4(0.0, 0.5, 0.5, 1.0),
  glm::vec4(0.8, 0.8, 0.8, 1.0),
  glm::vec4(0.2, 0.2, 0.2, 0.2),
  20.0
};

static glm::mat4 mvMatrix;
static glm::mat4 projMatrix;
static glm::mat3 normalMatrix;

static int err;

/* Grid geometry data object */
static GridData grid;

/* VBO names */
static GLuint buffers[NUM_BUFFERS];

/******************************************
	DEBUG FUNCTIONS
******************************************/

void printVec(float *v, int n)
{
  int i;

  for (i = 0; i < n; i++)
    printf("%4.2f ", v[i]);

  printf("\n");
}

void printMatrixLinear(float *m, int n)
{
  int i;

  for (i = 0; i < n; i++)
    printf("%4.2f ", m[i]);

  printf("\n");
}

void printMatrixColumnMajor(float *m, int n)
{
  int i, j;

  for (j = 0; j < n; j++)
  {
    for (i = 0; i < n; i++)
      printf("%5.2f ", m[i*4+j]);

    printf("\n");
  }
  printf("\n");
}

/******************************************
	BUFFER HANDLING
******************************************/

/* Generates the necessary buffers for VBO's */
void genBuffers (void)
{
	glGenBuffers (NUM_BUFFERS, buffers);
}

/* Buffers vertex and index data into the VBO's */
void bufferData (void)
{
	/* Insert vertex data into the bound vertex buffer */
	glBindBuffer (GL_ARRAY_BUFFER, buffers[VERTEX_BUF]);
	glBufferData (GL_ARRAY_BUFFER, sizeof (Vertex) * grid.vCount,
		grid.verts, GL_STATIC_DRAW);

	/* Insert element array index data into the bound index buffer */
	glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, buffers[INDEX_BUF]);
	glBufferData (GL_ELEMENT_ARRAY_BUFFER, sizeof (int) * grid.iCount,
		grid.indices, GL_STATIC_DRAW);
}

/* Unbinds the VBO buffers */
void unbindBuffers (void)
{
  int buffer;

  glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &buffer);
  if (buffer != 0)
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &buffer);
  if (buffer != 0)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

/* Enable vertex array rendering */
void enableVertexArrays (void)
{
	glEnableClientState (GL_VERTEX_ARRAY);
	glEnableClientState (GL_NORMAL_ARRAY);
}

/* Disable vertex array rendering */
void disableVertexArrays (void)
{
	glDisableClientState (GL_VERTEX_ARRAY);
	glDisableClientState (GL_NORMAL_ARRAY);
}

/******************************************
	GRID DATA
******************************************/

/* Calculates the positions of the vertices on the grid object. */
void calcVerts ()
{
	const float A1 = 0.25, k1 = 2.0 * M_PI, w1 = 0.25;
  const float A2 = 0.25, k2 = 2.0 * M_PI, w2 = 0.25;

	float t = g.t;

	for (int i = 0; i <= g.tess; ++i)
	{
		for (int j = 0; j <= g.tess; ++j)
		{
      int v = i * (g.tess + 1) + j;

      glm::vec3 n;
      glm::vec3 r = grid.verts[v].pos;

      // Calculate normals
      if (g.waveDim == 2)
      {
        n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
        n.y = 1.0;
        n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
        r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
      }
		  else
      {
        n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
        n.y = 1.0;
        n.z = 0.0;
        r.y = A1 * sinf(k1 * r.x + w1 * t);
      }

      grid.verts[v].pos = r;
			grid.verts[v].normal = n;
		}
	}
}

/* Initialises the grid data */
void initGrid (void)
{
	grid.vCount = (g.tess + 1) * (g.tess + 1);
	grid.iCount = grid.vCount * 2;

	// Realloc to handle resizing of grid
	grid.verts = (Vertex*)realloc (grid.verts, sizeof (Vertex) * grid.vCount);
	grid.indices = (int*)realloc (grid.indices, sizeof (int) * grid.iCount);

  float rowStep = 2.0f / (float) g.tess;
  float colStep = 2.0f / (float) g.tess;

  // Define grid vert positions and default normals
  for (int i = 0; i <= g.tess; ++i)
	{
		float x = -1.0 + i * colStep;
		for (int j = 0; j <= g.tess; ++j)
    {
			int v = i * (g.tess + 1) + j;
      grid.verts[v].pos = glm::vec3(x, 0.0, -1.0 + j * rowStep);
			grid.verts[v].normal = glm::vec3(0.0, 1.0, 0.0);
		}
	}

	// Init vertex element indices
	for (int i = 0, v = 0; i < g.tess; ++i)
	{
		for (int j = 0; j <= g.tess; ++j, v += 2)
		{
			int index = i * (g.tess + 1) + j;
			grid.indices[v] = index + g.tess + 1;
			grid.indices[v + 1] = index;
		}
	}

  if (g.fixed)
    calcVerts();
}

/* Executes functions necessary to perform grid tesselation change */
void resizeGrid (void)
{
	initGrid ();
	bufferData ();
}

/* Updates the grid vertices and rebuffers the data */
void updateGrid (void)
{
	calcVerts ();
	bufferData ();
}

/******************************************
	DRAWING
******************************************/

// Draw the world space origin axis
void drawAxes(float length)
{
  glm::vec3 v;

  GLint prog;
  glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
  glUseProgram (0);
  glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glBegin(GL_LINES);

  /* X */
  glColor3f(1.0, 0.0, 0.0);
  v = glm::vec3(0.0);
  glVertex3fv(&v[0]);
  v = glm::vec3(length, 0.0, 0.0);
  glVertex3fv(&v[0]);

  /* Y */
  glColor3f(0.0, 1.0, 0.0);
  v = glm::vec3(0.0, 0.0, 0.0);
  glVertex3fv(&v[0]);
  v = glm::vec3(0.0, length, 0.0);
  glVertex3fv(&v[0]);

  /* X */
  glColor3f(0.0, 0.0, 1.0);
  v = glm::vec3(0.0, 0.0, 0.0);
  glVertex3fv(&v[0]);
  v = glm::vec3(0.0, 0.0, length);
  glVertex3fv(&v[0]);

  glEnd();
  glPopAttrib();

  glUseProgram (prog);
}

// Draw a vector in immediate mode
void drawVector(glm::vec3 r, glm::vec3 v)
{
  glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);
  glDisable(GL_LIGHTING);
  glBegin(GL_LINES);

  glVertex3fv(&r[0]);
  glm::vec3 e(r + v);
  glVertex3fv(&e[0]);
  glEnd();
  glPopAttrib();
}

// Draws the sine wave using fixed or shader pipeline
void drawSineWave(int tess)
{
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable (GL_DEPTH_TEST);

  // Wirerame toggle
	if (g.wireframe)
  	glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

  // Smooth shading toggle
  if (g.smoothShade)
    glShadeModel(GL_SMOOTH);
  else
    glShadeModel(GL_FLAT);

  // Light and material properties
  glm::vec4 diffuse = sineWaveMat.diffuse;
  glm::vec4 spec = sineWaveMat.spec;
  glm::vec4 amb = sineWaveMat.amb;
  float gloss = sineWaveMat.gloss;
  glm::vec4 lightPos;
  if (g.pointLight)
    lightPos = glm::vec4(0.0, 1.0, 0.0, 1.0);
  else
    lightPos = glm::vec4(0.5, 0.5, 0.5, 0.0);

  if (g.fixed)
  {
    // Fixed pipeline lighting toggle
    if (g.lighting)
    {
      // Enable light
      glEnable(GL_LIGHTING);
      glEnable (GL_NORMALIZE);

      // Configure light
      GLfloat light_position[] = { lightPos.x, lightPos.y, lightPos.z, lightPos.w };
      glLightfv(GL_LIGHT0, GL_POSITION, light_position);
      glEnable(GL_LIGHT0);

      // Configure material
      GLfloat mat_diff[] = { diffuse.x, diffuse.y, diffuse.z, diffuse.w };
      GLfloat mat_spec[] = { spec.x, spec.y, spec.z, spec.w };
      glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diff);
      glMaterialfv(GL_FRONT, GL_SPECULAR, mat_spec);
      glMaterialf(GL_FRONT, GL_SHININESS, gloss);
    }
    else
    {
      glDisable(GL_LIGHTING);
      glDisable(GL_NORMALIZE);
      glColor3f(diffuse.x, diffuse.y, diffuse.z);
    }
  }
  else
  {
    // Configure lighting and shader properties
    glm::vec4 lPos = mvMatrix * lightPos;
    GLuint loc = glGetUniformLocation(g.shaderProgram, "_LightPos");
    glUniform4f(loc, lPos.x, lPos.y, lPos.z, lPos.w);

    loc = glGetUniformLocation(g.shaderProgram, "_Normals");
    glUniform1i(loc, g.drawNormals);

    loc = glGetUniformLocation(g.shaderProgram, "_BlinnPhong");
    glUniform1i(loc, g.blinn);

    loc = glGetUniformLocation(g.shaderProgram, "_FlatShade");
    glUniform1i(loc, g.smoothShade);

    loc = glGetUniformLocation(g.shaderProgram, "_VertexLight");
    glUniform1i(loc, !g.pixelLighting);

    loc = glGetUniformLocation(g.shaderProgram, "_PointLighting");
    glUniform1i(loc, g.pointLight);

    loc = glGetUniformLocation(g.shaderProgram, "_Lighting");
    glUniform1i(loc, g.lighting);

    loc = glGetUniformLocation(g.shaderProgram, "_WaveDim");
    glUniform1i(loc, g.waveDim);

    loc = glGetUniformLocation(g.shaderProgram, "_Gloss");
    glUniform1f(loc, gloss);

    loc = glGetUniformLocation(g.shaderProgram, "_Diffuse");
    glUniform4f(loc, diffuse.x, diffuse.y, diffuse.z, diffuse.w);

    loc = glGetUniformLocation(g.shaderProgram, "_Spec");
    glUniform4f(loc, spec.x, spec.y, spec.z, spec.w);

    loc = glGetUniformLocation(g.shaderProgram, "_Ambient");
    glUniform4f(loc, amb.x, amb.y, amb.z, amb.w);
  }

  enableVertexArrays ();

	glBindBuffer (GL_ARRAY_BUFFER, buffers[VERTEX_BUF]);
  glVertexPointer (3, GL_FLOAT, sizeof (Vertex), BUFFER_OFFSET(0));
	glNormalPointer (GL_FLOAT, sizeof (Vertex), BUFFER_OFFSET(sizeof (glm::vec3)));

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, buffers[INDEX_BUF]);

  // Draw sine wave
	for (int i = 0; i < tess; i++)
	{
  	glDrawElements (GL_TRIANGLE_STRIP, (tess + 1) * 2, GL_UNSIGNED_INT,
			BUFFER_OFFSET(i * (tess + 1) * 2 * sizeof(unsigned int)));
	}

	disableVertexArrays();

  // Draw normals in fixed mode
  if (g.fixed && g.drawNormals)
  {
    glPushAttrib(GL_CURRENT_BIT);
    glColor3f(1.0, 1.0, 0.0);
    for (int i = 0; i < grid.vCount; ++i)
    {
      drawVector(grid.verts[i].pos, grid.verts[i].normal * 0.05f);
    }
    glPopAttrib();
  }

  while ((err = glGetError()) != GL_NO_ERROR)
  {
    printf("%s %d\n", __FILE__, __LINE__);
    printf("displaySineWave: %s\n", gluErrorString(err));
  }
}

/******************************************
	PERF METER
******************************************/

// Console performance meter
void consolePM()
{
  printf("frame rate (f/s):  %5.0f\n", g.frameRate);
  printf("frame time (ms/f): %5.0f\n", 1.0 / g.frameRate * 1000.0);
  printf("tesselation:       %5d\n", g.tess);

  if (g.blinn)
    puts("Refl model: Blinn-Phong");
  else
    puts("Refl model: Phong");

  if (!g.pointLight)
    puts("Light type: Dir");
  else
    puts("Light type: Point");

  if (g.pixelLighting)
    puts("Lighting calc: Per pixel");
  else
    puts("Lighting calc: Per vertex");

  printf("Shininess: %2.0f\n", sineWaveMat.gloss);

  if (g.fixed)
    puts("Pipeline: Fixed");
  else
    puts("Pipeline: Programmable");

}

// On screen display
void displayOSD()
{
  char buffer[30];
  char *bufp;
  int w, h;

  GLint prog;
  glGetIntegerv(GL_CURRENT_PROGRAM, &prog);
  glUseProgram (0);

  glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  /* Set up orthographic coordinate system to match the window,
  i.e. (0,0)-(w,h) */
  w = glutGet(GLUT_WINDOW_WIDTH);
  h = glutGet(GLUT_WINDOW_HEIGHT);

  glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  /* Frame rate */
  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i(10, 60);
  snprintf(buffer, sizeof buffer, "frame rate (f/s):  %5.0f", g.frameRate);
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

  /* Frame time */
  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i(10, 40);
  snprintf(buffer, sizeof buffer, "frame time (ms/f): %5.0f", 1.0 / g.frameRate * milli);
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

  /* Tesselation */
  glColor3f(1.0, 1.0, 0.0);
  glRasterPos2i(10, 20);
  snprintf(buffer, sizeof buffer, "tesselation:       %5d", g.tess);
  for (bufp = buffer; *bufp; bufp++)
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

  // Display extra information
  if (g.osdType == 2)
  {
    /* Reflectance model */
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 80);
    if (g.blinn)
      snprintf(buffer, sizeof buffer, "%s", "Refl model: Blinn-Phong");
    else
      snprintf(buffer, sizeof buffer, "%s", "Refl model: Phong");
    for (bufp = buffer; *bufp; bufp++)
      glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    /* Light type */
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 100);
    if (!g.pointLight)
      snprintf(buffer, sizeof buffer, "%s", "Light type: Dir");
    else
      snprintf(buffer, sizeof buffer, "%s", "Light type: Point");
    for (bufp = buffer; *bufp; bufp++)
      glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    /* Lighting type */
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 120);
    if (g.pixelLighting)
      snprintf(buffer, sizeof buffer, "%s", "Lighting calc: Per pixel");
    else
      snprintf(buffer, sizeof buffer, "%s", "Lighting calc: Per vertex");
    for (bufp = buffer; *bufp; bufp++)
      glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    /* Shininess */
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 140);
    snprintf(buffer, sizeof buffer, "Shininess: %5.0f", sineWaveMat.gloss);
    for (bufp = buffer; *bufp; bufp++)
      glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

    /* Pipeline type */
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 160);
    if (g.fixed)
      snprintf(buffer, sizeof buffer, "%s", "Pipeline: Fixed");
    else
      snprintf(buffer, sizeof buffer, "%s", "Pipeline: Programmable");
    for (bufp = buffer; *bufp; bufp++)
      glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
  }

  glPopMatrix();  /* Pop modelview */
  glMatrixMode(GL_PROJECTION);

  glPopMatrix();  /* Pop projection */
  glMatrixMode(GL_MODELVIEW);

  glPopAttrib();

  glUseProgram(prog);
}

/******************************************
	GLUT CALLBACKS
******************************************/

void init(void)
{
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glEnable(GL_DEPTH_TEST);

  genBuffers();
	initGrid();
	bufferData();
	unbindBuffers();

  /* Compile and set shader program for rendering */
  g.shaderProgram = getShader("shader.vert", "shader.frag");
}

void reshape(int w, int h)
{
  g.width = w;
  g.height = h;
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
}

void idle()
{
  float t, dt;

  t = glutGet(GLUT_ELAPSED_TIME) / milli;

  g.realT = t;

  // Accumulate time if animation enabled
  if (g.animate)
  {
    dt = t - g.lastT;
    g.t += dt;
    g.lastT = t;

    if (!g.fixed)
    {
      // Set time value in shader program for aniation
      GLuint id = glGetUniformLocation(g.shaderProgram, "_Time");
      glUniform1f(id, g.t);
    }
    else
    {
      // Animate sine wave on CPU
      updateGrid();
    }
  }

  // Update stats, although could make conditional on a flag set interactively
  dt = (t - g.lastStatsDisplayT);
  if (dt > g.displayStatsInterval) {
    g.frameRate = g.frameCount / dt;
    if (debug[d_OSD])
      printf("dt %f framecount %d framerate %f\n", dt, g.frameCount, g.frameRate);
    if (g.consolePM)
      consolePM();
    g.lastStatsDisplayT = t;
    g.frameCount = 0;
  }

  glutPostRedisplay();
}

void display()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(0, 0, g.width, g.height);

  /* Set up matrices */
  // Model view matrix
  mvMatrix = glm::mat4(1.0);
  mvMatrix = glm::rotate(mvMatrix, camera.rotateX * glm::pi<float>() / 180.0f, glm::vec3(1.0, 0.0, 0.0));
  mvMatrix = glm::rotate(mvMatrix, camera.rotateY * glm::pi<float>() / 180.0f, glm::vec3(0.0, 1.0, 0.0));
  mvMatrix = glm::scale(mvMatrix, glm::vec3(camera.scale));
  // Normal matrix
  normalMatrix = glm::transpose(glm::inverse(glm::mat3(mvMatrix)));
  // Projection matrix
  projMatrix = glm::ortho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

  // Apply matrices for both fixed and programmable pipelines
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(&projMatrix[0][0]);
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(&mvMatrix[0][0]);

  if (!g.fixed)
  {
    GLuint loc = glGetUniformLocation(g.shaderProgram, "_ModelViewMatrix");
    glUniformMatrix4fv(loc, 1, GL_FALSE, &mvMatrix[0][0]);
    loc = glGetUniformLocation(g.shaderProgram, "_NormalMatrix");
    glUniformMatrix3fv(loc, 1, GL_FALSE, &normalMatrix[0][0]);
    loc = glGetUniformLocation(g.shaderProgram, "_ProjMatrix");
    glUniformMatrix4fv(loc, 1, GL_FALSE, &projMatrix[0][0]);
  }

  drawSineWave(g.tess);
  drawAxes(5.0);

  if (g.osdType != 0)
    displayOSD();

  glutSwapBuffers();

  g.frameCount++;

  while ((err = glGetError()) != GL_NO_ERROR)
  {
    printf("%s %d\n", __FILE__, __LINE__);
    printf("display: %s\n", gluErrorString(err));
  }
}

void keyboard(unsigned char key, int x, int y)
{
  switch (key)
  {
    case 27:
      // Exit
      exit(0);
      break;
    case 'a':
      // Toggle animation
      g.animate = !g.animate;
      if (g.animate)
        g.lastT = glutGet(GLUT_ELAPSED_TIME) / milli;
      break;
    case 'l':
      // Toggle lighting
      g.lighting = !g.lighting;
      glutPostRedisplay();
      break;
    case 'p':
      // Toggle vertex or per pixel lighting
      if (!g.fixed)
      {
        g.pixelLighting = !g.pixelLighting;
        if (g.pixelLighting)
          puts("Pixel lighting enabled.");
        else
          puts("Vertex lighting enabled.");
        glutPostRedisplay();
      }
      else
      {
        puts ("Fixed pipeline does not support pixel lighting.");
      }
      break;
    case 'f':
      // Toggle smooth and flat shading
      if (!g.fixed)
      {
        if (!g.pixelLighting)
        {
          puts ("Flat shading not supported in vertex shader. Switch to pixel.");
          break;
        }
      }
      g.smoothShade = !g.smoothShade;
      if(g.smoothShade)
        puts("Smooth shading enabled.");
      else
        puts("Smooth shading disabled.");
      glutPostRedisplay ();
      break;
    case 'w':
      g.wireframe = !g.wireframe;
      glutPostRedisplay();
      break;
    case 'n':
      // Toggle normal rendering
      g.drawNormals = !g.drawNormals;
      glutPostRedisplay();
      break;
    case 'm':
      // Toggle blinn-phong or phong shading
      if (g.fixed && g.blinn)
      {
        puts("Fixed pipeline does not support phong shading.");
        break;
      }
      g.blinn = !g.blinn;
      if (g.blinn)
        puts("Blinn shading enabled.");
      else
        puts("Phong shading enabled.");
      glutPostRedisplay();
      break;
    case 'c':
      // Toggle console performance meter
      g.consolePM = !g.consolePM;
      glutPostRedisplay();
      break;
    case 'o':
      // Cycle through OSD types
      g.osdType ++;
      if (g.osdType == 3)
        g.osdType = 0;
      glutPostRedisplay();
      break;
    case 's':
      // Toggle shader usage
      g.fixed = !g.fixed;
      if(g.fixed)
      {
        glUseProgram(0);
        puts("Fixed pipeline enabled.");
      }
      else
      {
        glUseProgram(g.shaderProgram);
        puts("Programmable pipeline enabled.");
      }
      initGrid ();
      bufferData ();
      glutPostRedisplay();
      break;
    case 'T':
      // Increase tesselation
      g.tess *= 2;
      resizeGrid ();
      glutPostRedisplay();
      break;
    case 'H':
      // Increase shine
      if (sineWaveMat.gloss < 60.0)
      {
        sineWaveMat.gloss += 2.0;
        glutPostRedisplay();
      }
      break;
    case 'h':
      // Decrease shine
      if (sineWaveMat.gloss > 4.0)
      {
        sineWaveMat.gloss -= 2.0;
        glutPostRedisplay();
      }
      break;
    case 't':
      // Decrease tesselation
      g.tess /= 2;
      if (g.tess < 8)
      g.tess = 8;
      resizeGrid ();
      glutPostRedisplay();
      break;
    case 'd':
      // Toggle between dir and pos light type
      g.pointLight = !g.pointLight;
      if(g.pointLight)
        puts("Point lighting enabled.");
      else
        puts("Directional lighting enabled.");
      glutPostRedisplay();
      break;
    case 'z':
      // Toggle wave dimension
      if (g.waveDim == 1)
        g.waveDim = 2;
      else
        g.waveDim = 1;
      updateGrid();
      break;
    default:
      break;
  }
}

void mouse(int button, int state, int x, int y)
{
  if (debug[d_mouse])
    printf("mouse: %d %d %d\n", button, x, y);

  camera.lastX = x;
  camera.lastY = y;

  if (state == GLUT_DOWN)
  {
    switch(button)
    {
      case GLUT_LEFT_BUTTON:
      camera.control = rotate;
      break;
      case GLUT_MIDDLE_BUTTON:
      camera.control = pan;
      break;
      case GLUT_RIGHT_BUTTON:
      camera.control = zoom;
      break;
    }
  }
  else if (state == GLUT_UP)
  {
    camera.control = inactive;
  }
}

void motion(int x, int y)
{
  float dx, dy;

  if (debug[d_mouse])
  {
    printf("motion: %d %d\n", x, y);
    printf("camera.rotate: %f %f\n", camera.rotateX, camera.rotateY);
    printf("camera.scale:%f\n", camera.scale);
  }

  dx = x - camera.lastX;
  dy = y - camera.lastY;
  camera.lastX = x;
  camera.lastY = y;

  switch (camera.control)
  {
    case inactive:
    break;
    case rotate:
    camera.rotateX += dy;
    camera.rotateY += dx;
    break;
    case pan:
    break;
    case zoom:
    camera.scale += dy / 100.0;
    break;
  }

  glutPostRedisplay();
}

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize (1024, 1024);
  glutInitWindowPosition (100, 100);
  glutCreateWindow (argv[0]);
  init();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutIdleFunc(idle);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutMainLoop();
  return 0;
}
