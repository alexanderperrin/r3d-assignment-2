/* shader.vert
 * 
 * Real-Time Rendering & 3D Games Programming
 * Semester 2 2015 Assignment #2
 * Full Name        : Alexander Perrin
 * Student Number   : s3333342
 * Program Code     : BP214
 */
 
#extension GL_EXT_gpu_shader4 : require

// Sinewave helpers
#define M_PI 3.1415926535897932384626433832795
#define A 0.25
#define W 0.25
#define K 2.0 * M_PI

// Uniforms
uniform float _Time; // Animation time
uniform mat4 _ModelViewMatrix;
uniform mat4 _ProjMatrix;
uniform mat3 _NormalMatrix;
uniform vec4 _LightPos; // Point light position
uniform vec4 _Diffuse; // Diffuse color
uniform vec4 _Spec; // Specular color
uniform vec4 _Ambient; // Ambient color
uniform bool _VertexLight; // Vertex lighting toggle
uniform bool _BlinnPhong; // Blinn-Phong / Phong lighting toggle
uniform bool _Normals; // Normal visualisation toggle
uniform bool _PointLighting; // Point lighting toggle
uniform bool _FlatShade; // Flad shading toggle
uniform bool _Lighting; // Lighting toggle
uniform float _Gloss; // Glossiness
uniform int _WaveDim; // Wave dimensions

// Varying variables for fragment program
flat varying vec3 fNormal; // Flat out normal
varying vec3 normal; // Smooth out normal
varying vec4 ePos; // Out eye space position
varying vec4 vColor; // Out color data for vertex lighting
varying vec4 lightDir; // Out directional light direction

vec4 blinnPhongSpec(vec3 n, vec3 l, vec3 e)
{
  vec3 h = normalize(l + e);
  float intSpec = max(dot(h,n), 0.0);  
  return _Spec * pow(intSpec, _Gloss);
}

vec4 phongSpec(vec3 n, vec3 l, vec3 e)
{
  vec3 r = 2.0 * dot(l, n) * n - l;
  float intSpec = max(dot(r,e), 0.0);  
  return _Spec * pow(intSpec, _Gloss);
}

void main(void)
{
  vec4 pos = gl_Vertex;

  // Do sine wave calculation in local object space
  pos.y = A * sin(K * pos.x + W * _Time) + A * sin(K * pos.z + W * _Time);
  normal.x = -A * K * cos(K * pos.x + W * _Time);
  normal.y = 1.0;
  normal.z = -A * K * cos(K * pos.z + W * _Time);
  
  if (_WaveDim == 2)
  {
    pos.y = A * sin(K * pos.x + W * _Time) + A * sin(K * pos.z + W * _Time);
    normal.x = -A * K * cos(K * pos.x + W * _Time);
    normal.y = 1.0;
    normal.z = -A * K * cos(K * pos.z + W * _Time);
  }
  else
  {
    pos.y = A * sin(K * pos.x + W * _Time);
    normal.x = -A * K * cos(K * pos.x + W * _Time);
    normal.y = 1.0;
    normal.z = 0.0;
  }

  // Transform vertex into eye space
  ePos = _ModelViewMatrix * pos;
  
  // Set up varying variables for fragment program
  normal = normalize(_NormalMatrix * normal);
  fNormal = normal;
  
  if (_LightPos.w > 0.0)
  {
    // Point lighting
    lightDir = normalize(_LightPos - ePos);
  }
  else
  {
    // Directional lighting
    lightDir = normalize(_LightPos);
  }
  
  if (_VertexLight)
  {
    if (_Lighting)
    {
      vec3 n = vec3(0);
      if (_FlatShade)
        n = normalize(normal);
      else
        n = normalize(fNormal);
      vec3 l = vec3(lightDir);
      vec3 e = vec3(0.0, 0.0, 1.0);
      
      float intensity = max(dot(n,l), 0.0);
      vec4 spec = vec4(0);
      
      if (intensity > 0.0)
      {
        if (_BlinnPhong)
        {
          spec = blinnPhongSpec(n, l, e);
        }
        else
        {
          spec = phongSpec(n, l, e);
        }
      }
      vColor = _Diffuse * intensity + spec + _Ambient * _Ambient;
    }
    else
    {
      vColor = _Diffuse;
    }
    if (_Normals)
    {
      vColor = vec4(normal, 1.0);
    }
  }
  
  gl_Position = _ProjMatrix * ePos;
}
